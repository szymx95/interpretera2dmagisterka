import math

class Point:
    """Klasa opisująca punkt w przestrzeni

    Parametry:
        X (float): Współrzędna X punktu.
        Y (float): Współrzędna Y punktu.
    """
    def __init__(self,X,Y):
        """Konstruktor klasy Point

        Args:
            X (float): Współrzędna X punktu.
            Y (float): Współrzędna Y punktu.
        """
        self.X = X
        self.Y = Y

    def __eq__(self,other):
        """Dwa obiekty klasy punkt są równe jeżeli ich współrzędne są równe.

        Args:
            other (Point): Punkt z którym jest porównywany obiekt

        Returns:
            boolean: Zwraca wartość bool mówiąca czy dwa punkty są sobie równe.
        """
        if type(other) == type(None):
            return False
        if type(other) == str:
            return False
        return self.X == other.X and self.Y == other.Y

    def calculateDistance(self,other):
        """Zwraca odległość między dwoma punktami.

        Returns:
            float: Odległość między punktami.
        """
        return math.sqrt( (self.X - other.X)**2 + (self.Y - other.Y)**2 )
        
    def getClosestNorthSegmentFromList(self,segments):
        """Funkcja zwraca najbliższy odcinek który jest przecinany przez półprostą o początku w punkcie i wektorze [0,1].
        Czyli szuka najbliższego odcinka który znajduje się w Y+ od punktu.

        Args:
            segments (list[Segment]): Lista odcinków które mają być sprawdzone względem punktu.

        Returns:
            Segment: Zwraca najbliższy odcinek. Jeżeli odcinek nie zostanie znaleziony zwraca None.
        """
        def checkSegment(seg,closestPoint):
            """Sprawdza czy odcinek przecinany jest przez półprostą, jeżeli tak to sprawdza czy jest bliżej niż najbliższy punkt.

            Args:
                seg (Segment): Sprawdzany odcinek.
                closestPoint (Point): Najbliższy dotychczasowy punkt.

            Returns:
                boolean: Zwraca True jeżeli segment jest bliżej niż podany punkt i leży na półprostej, w przeciwnym wypadku False.
            """

            segmentPoint = seg.getLine().getValue(self.X) #Do segmentPoint przypisywana jest wartość jaką ma linia na której leży odcinek we współrzędnej X punktu.
            if segmentPoint == None:
                return False
            if segmentPoint == 'inf':                     #Jeżeli linia jest pionowa i leżt na współrzędnej X zwracana wartość to 'inf'                                                       
                    segmentPoint = seg.getMiddle()        
            if segmentPoint.Y > self.Y:                   #Najpierw sprawdzane jest czy odcinek znajduje się na Y dodatnim
                if seg.isPointInSegment(segmentPoint):    #Jeżeli tak to sprawdzane jest czy punkt leży na odcinku i czy jest bliżej od najbliższego punktu.
                    if closestPoint == None:
                        return True
                    if segmentPoint.Y < closestPoint.Y:
                        return True
            return False


        #Ze względu na to, że występują krzywe należy sprawdzać czy linia przecina krawędź od krawędzi odcinka do środka i od środka do drugiej krawędzi.
        # funkcja getMiddle() odcinków zwraca środek krzywej.
        
        
        closestPoint = None
        closestSegment = None
        for segment in segments:            
            for seg in segment.approximateSegments():
                if  checkSegment(seg,closestPoint):
                    closestPoint = seg.getLine().getValue(self.X)
                    if closestPoint == 'inf':
                        closestPoint = seg.getMiddle()
                    closestSegment = segment
        return closestSegment

class Segment:
    """Klasa opisuje odcinki geometryczne

    Parametry:
        p1 (Point): Pierwszy punkt odcinka.
        p2 (Point): Drugi punkt odcinka.
        middle(Point): Punkt środka odcinka.
    """
    def __init__(self,p1,p2):
        """Konstruktor klasy Segment. Opisuje ona odcinki w przestrzeni 2D. 
        Konstruktor przyjmuje 2 Punkty które stanowią krawędzie odcinka.


        Args:
            p1 (Point): Pierwszy punkt odcinka
            p2 (Point): Drugi punkt odcinka
        """
        self.p1 = p1
        self.p2 = p2
        self.middle = Point(X=(self.p1.X+self.p2.X)/2, Y=(self.p1.Y+self.p2.Y)/2)

    def getMiddle(self):
        return self.middle

    def getLength(self):
        """Funkcja zwraca długość odcinka

        Returns:
            float: Długość odcinka
        """
        return self.p1.calculateDistance(self.p2)

    def getNextRightSegment(self,edgePoint,edgeList):
        """Funkcja wykorzystywana do wyszukiania odcinka który posiada punkt wspólny z odcinkiem wywoływanego obiektu.
        Jeżeli odcinków jest więcej niż jeden zwracany jest odcinek którego kąt liczony między obiektem a odcinkiem zgodnie z ruchem wskazówek zegara jest największy.

        Args:
            edgePoint (Point): Punkt wyszukiwania wspólnego odcinka. Musi należeć do odcinka z którego wywoływana jest funkcja.
            edgeList (list[Segment]): Lista odcinków wśród których ma być znaleziony odcinek.

        Returns:
            list[Point,Segment]: Przeciwny punkt znalezionego odcinka, oraz znaleziony odcinek.
        """
        if self.p1 != edgePoint and self.p2 != edgePoint:       #Jeżeli punkt nie należy do odcinka zwróć pustą listę
            return []
        edges = []
        for edge in edgeList:
            if edge.p1 != self.p1 or edge.p2 != self.p2:
                if edge.p1 == edgePoint or edge.p2 == edgePoint:
                    edges.append(edge)                          #Najpierw tworzona jest lista krawędzi którego współdzielą punkt

        Vec = Vector(edgePoint,self.getOtherPoint(edgePoint))

        BiggestAngle = 0
        BiggestPoint = None
        for edge in edges:
            angle = Vec.angleClockwise( Vector(edgePoint,edge.getMiddle()) ) #Wyliczany jest kąt między odcinkiem szukanym a bazowym i zwracany jest ten o największym kącie.
            if angle > BiggestAngle:
                BiggestAngle = angle
                BiggestPoint = [edge.getOtherPoint(edgePoint),edge]
        return BiggestPoint

    def getRightPoint(self):
        """Zwraca punkt którego współrzędna X jest większa. Jeżeli współrzędne X obu punktów są te same zwracany jest ten o większej współrzędnej Y.

        Returns:
            Point: Punkt znajdujący się na prawo od drugiego punktu.
        """
        if self.p1.X == self.p2.X:
            return self.p1 if self.p1.Y > self.p2.Y else self.p2
        else:
            return self.p2 if self.p2.X > self.p1.X else self.p1

    def getOtherPoint(self,p):
        """Zwraca punkt który nie jest punktem p.

        Args:
            p (Point): Punkt referencyjny.

        Returns:
            Point: Punkt który nie jest punktem referencyjnym.
        """
        return self.p1 if p == self.p2 else self.p2

    def getBisection(self):
        """Zwraca Linie która jest prostą prostopadłą do odcinka

        Returns:
            Line: Prosta prostopadła do odcinka.
        """
        if self.p1.Y == self.p2.Y:
            return Line("inf",self.middle.X)
        slope = -(self.p1.X-self.p2.X)/(self.p1.Y-self.p2.Y)    
        c = self.middle.Y - self.middle.X*slope
        return Line(slope,c)

    def getLine(self):
        """Zwraca Linie na której leży odcinek

        Returns:
            Line: Linia na której leży odcinek.
        """
        return Line(p1=self.p1,p2=self.p2)

    def isPointInSegment(self,point):
        """Sprawdza czy punkt lezy na odcinku.

        Args:
            point (Point): Sprawdzany punkt

        Returns:
            boolean: True jeżeli punkt znajduje się w odcinku, False w przeciwnym wypadku.
        """
        #Jeżeli prosta na której leży odcinek jest pionowa i nie leży na współrzędnej X zwraca None, jeżeli leży na X zwraca 'inf'
        IntersectionPoint = self.getLine().getValue(point.X)
        if IntersectionPoint == None:
            return False
        if IntersectionPoint == 'inf':
            return min(self.p1.Y,self.p2.Y) <= point.Y <= max(self.p1.Y,self.p2.Y)

        return IntersectionPoint == point and min(self.p1.X,self.p2.X) <= point.X <= max(self.p1.X,self.p2.X)
        

class Line:
    """Klasa Line opisuje prostą w przestrzeni dwuwymiarowej.

    Parametry:
        slope (float): Wartość nachylenia prostej zgodnie z równaniem Y = slope*X+c. Jeżeli prosta jest pionowa parametr ten przyjmuje wartość 'inf'.
        c (float): Tak jak wyżej parametr opisujący równanie prostej. Jeżeli prosta jest pionowa parametr c opisuje współrzędną X prostej.
    """
    def __init__(self,slope=None,c=None,p1=None,p2=None):
        """Konstruktor obiektów Line.
            Może być zainicjowana na dwa sposoby:
                -Poprzez jawne podanie wartości slope oraz c zgodnie z równaniem Y = slope*X+c
                -Poprzez podanie dwóch punktów przez który ma przechodzić prosta. W tym wypadku parametry slope i c zostaną wyliczone.
            Jeżeli podane zostaną wszystkie parametry pierwszeństwo biorą parametry p1 i p2.

        Args:
            slope (Float, optional): Parametr opisujący nachylenie. Defaults to None.
            c (float, optional): Parametr opisujący równanie prostej. Defaults to None.
            p1 (Point, optional): Punkt przez który ma przechodzić prosta. Defaults to None.
            p2 (Point, optional): Punkt przez który ma przechodzić prosta. Defaults to None.
        """
        if p1 != None and p2 != None:
            if p1.X == p2.X:
                self.slope = 'inf'
                self.c = p1.X
            else:
                self.slope = (p1.Y-p2.Y)/(p1.X-p2.X)
                self.c = (p1.Y - self.slope * p1.X )
        else:
            self.slope = slope
            self.c = c

    def getCommonPointWithLine(self,other):
        """Funkcja zwraca punkt przecięcia się prostej z inną prostą. 
        Jeżeli proste są równoległe i leżą na sobie zwracany jest punkt któy leży na X równym parametrowi c prostej.
        Jeżeli proste są równoległe i na sobie nie leżą, zwracane jest None. 
        Args:
            other (Line): Prosta przecinająca się z prostą z której wywoływana jest funkcja.

        Returns:
            [Point]: Punkt przecięcia się prostych None jeżeli punkty się nie przecinają.
        """
        if self.slope==other.slope:
            if self.c == other.c:
                return self.getValue(self.c)
            else:
                return None
            
        if self.slope == "inf":
            return other.getValue(self.c)
        elif other.slope == "inf":
            return self.getValue(other.c)
        else:
            return self.getValue( (other.c-self.c)/(self.slope-other.slope) )
    
    def getValue(self,X):
        """Zwraca punkt prostej we współrzędnej X.

        Args:
            X (float): Współrzędna X dla której ma być obliczona wartość.

        Returns:
            Point: Punkt leżący na prostej we współrzędnej X.
        """
        if self.slope == 'inf':
            if X == self.c:
                return 'inf'
            else:
                return None
        return Point(X,self.slope * X + self.c)

class Circle(Point):
    """Klasa opisuje okrąg, dziedziczy po klasie Point.

    Parametry:
        radius (float): Promień okręgu.
    """
    def __init__(self,point,R):
        """Konstruktor klasy Circle.

        Args:
            point (Point): Punkt środka okręgu
            R (float): Promień okręgu
        """
        super(Circle, self).__init__(point.X,point.Y)
        self.radius = R  

    def __eq__(self,other):
        """Dwa obiekty klasy Circle są równe jeżeli ich środki oraz promienie są takie same.

        Args:
            other (Circle): Okrąg do porównania.

        Returns:
            boolean: True jeżeli okręgi są takie same, False w przeciwnym wypadku.
        """
        if other == None:
            return False
        return self.X == other.X and self.Y == other.Y and self.radius == other.radius

    def getValue(self,X):
        """Zwraca punkty na okręgu we współrzędnych X.
        Jeżeli X jest poza okręgiem zwraca None.
        Args:
            X (float): Współrzędna X której wartość okręgu ma być zwrócona.

        Returns:
            tuple(Point,Point): Punkty we współrzędnych X
        """
        if -self.X^2 + 2*x*self.X  + self.radius^2 - X^2 < 0:
            return None
        delta = sqrt(-self.X^2 + 2*x*self.X  + self.radius^2 - X^2)
        y1 = self.Y - delta
        y2 = self.Y + delta

        return (Point(X,y1),Point(X,y2))

    def getCommonPointsWithLine(self,line):
        """Zwraca wspólne punkty okręgu z prostą. 
        Jeżeli prosta nie przecina okręgu zwracana jest wartość None.
        Jeżeli punkt przecięcia jest tylko jeden oba punkty będą te same.

        Args:
            line (Line):Prosta która której punkty przecięci są szukane.

        Returns:
            [tuple(Point,Point)]: Tupla 2 znalezionych punktów przecinających okrąg.
        """

        m = line.slope
        c = line.c
        if m == "inf":
            delta = -(self.X**2) +2*self.X*c + self.radius**2 - c**2
            if delta < 0:
                return None
            delta = math.sqrt(delta)
            return Point(X= c, Y= delta+ self.Y), Point(X= c, Y= -delta+ self.Y)
        delta = -self.X**2*m**2 + 2*self.Y*(self.X*m + c) - 2*self.X*c*m - self.Y**2 - c**2 + m**2*self.radius**2 + self.radius**2
        if delta < 0:
            return None
        delta = math.sqrt(delta)

        xCurve0 = (delta + self.X + self.Y*m - c*m)/(m**2 + 1)
        yCurve0 = m*xCurve0+c

        xCurve1 = (-delta + self.X + self.Y*m - c*m)/(m**2 + 1)
        yCurve1 = m*xCurve1+c
        return Point(X=xCurve0,Y=yCurve0),Point(X=xCurve1,Y=yCurve1)

class AgrosCircle(Circle):
    """Klasa opisująca okrąg w Agrosie. Dziedziczy po klasie Circle i rózni się tylko posiadaniem parametru id.

    Parameters:
        id (int): id okręgu.
    """
    def __init__(self,_id,p,radius):
        super(AgrosCircle, self).__init__(p,radius)
        self.id = _id

class AgrosPoint(Point):
    """Klasa opisująca punkt w Agrosie. Dziedziczy po klasie Point i rózni się tylko posiadaniem parametru id.

    Parameters:
        id (int): id punktu.
    """
    def __init__(self,_id,x,y):
        super(AgrosPoint, self).__init__(x,y)
        self.id = _id

class AgrosLabel(Point):
    """Klasa opisująca etykietę w Agrosie. Dziedziczy po klasie Point.

    Parameters:
        id (int): id etykiety.
        material (dict): Słownik opisujący materiał przypisany do etykiety.
    """
    def __init__(self,_id,x,y,material=None):
        super(AgrosLabel, self).__init__(x,y)
        self.id = _id
        self.material = material


class AgrosSegment(Segment):
    """Odcinek w programie Agros2D.
    Dziedziczy po klasie Segment.

    Parameters:
        id (int): id etykiety.
        angle (float): Wartość kąta krawędzi.
        boundary (dict): Słownik zawierający informacje na temat warunku brzegowego przypisanego do krawędzi.
        middleOfCurve (Point): Punkt środka krzywej.
        circle (AgrosCircle): Okrąg na której znajduje się krzywa.
    """
    def __init__(self,_id,p1,p2,angle=0,boundary=None):
        """Konstruktor obiektu klasy AgrosSegment.

        Args:
            _id (int): id odcinka.
            p1 (AgrosPoint): Punkt krawędzi odcinka.
            p2 (AgrosPoint): Punkt krawędzi odcinka.
            angle (float, optional): Wartość kąta odcinka. Defaults to 0.
            boundary (dict, optional): Słownik z informacjami na temat warunku brzegowego odcinka. Defaults to None.
        """
        super(AgrosSegment, self).__init__(p1,p2)
        self.id = _id
        self.angle=angle
        self.boundary = boundary
        self.middleOfCurve = self.middle
        self.circle = None
    def getMiddle(self):
        """Funkcja zwraca punkt środka odcinka i nadpisuje funkcję rodzica. Jeżeli odcinek jest krzywą zwraca wartość środka krzywej.

        Returns:
            Point: Punkt środka odcinka, lub krzywej.
        """
        if self.angle == 0:
            return super().getMiddle()
        else:
            return self.middleOfCurve

    def approximateSegments(self):
        """Funkcja przybliża krzywą za pomocą wielu mniejszych segmentów.
        Jeżeli odcinek nie jest krzywą zwraca siebie.

        Returns:
            list[Segment]: Lista odcinków aproksymujących krzywą.
        """
        def divide(p1,p2,n):
            if n == 4:
                return True
            possibleCircles = self.circle.getCommonPointsWithLine( Segment(p1,p2).getBisection() )
            for e in possibleCircles:
                if min(p1.X,p2.X,self.middleOfCurve.X) <= e.X <= max(p1.X,p2.X,self.middleOfCurve.X) and min(p1.Y,p2.Y,self.middleOfCurve.Y) <= e.Y <= max(p1.Y,p2.Y,self.middleOfCurve.Y):                    
                    divide(p1,e,n+1)   
                    approxSeg.append(e)                 
                    divide(e,p2,n+1)
                    

        if self.angle == 0:
            return [self]
        
        approxSeg = [self.p1]
        
        divide(self.p1,self.p2,1)

        approxSeg.append(self.p2)

        returnSegments = []

        prevSegment = self.p1
        for e in approxSeg[1:]:
            returnSegments.append(Segment(prevSegment,e))
            prevSegment = e

        return returnSegments
    def calculateCircle(self,circles):
        """Funkcja oblicza punkt środka krzywej oraz okrąg na której znajduje się krzywa na podstawie kąta angle.

        Args:
            circles (dict[AgrosCircle]): Słownik okręgów programu Agros. Jeżeli obliczony okrąg już istnieje zostanie on przypisany do parametru circle obiektu.
            w przeciwnym wypadku zostanie stworzony nowy obiekt i do argumentu circles zostanie dodany nowy okrąg.
        """
        if self.angle == 0:
            return None
                                                #skoro brany jest pod uwagę środek krzywej punkty [p1, śordek krzywej, p2] tworzą trójkąt równoramienny.
        c = self.getLength()
        alfa = math.radians(180 - self.angle/2) #Wyliczane jest jaka jest wartość kąta [p1, środek krzywej, p2]

        l = math.sqrt((c**2)/( (1-math.cos(alfa))*2 )) #Wyliczana jest wartość odległości środka krzywej od krawędzi odcinków za pomocą twierdzenia cosinusów
                                                        #ogolne twierdzenie c^2 = a^2 + b^2 - 2ab cos alfa
                                                        #w tym przypadku ramiona są równe i są szukane więc równanie sprowadza się do postaci
                                                        #         (      c^2        )
                                                        # l = sqrt(-----------------)
                                                        #         (  2(1-cos(alfa)) )
        #punkt środka krzywej jest jednym z punktów który tworzony jest poprzez przecięcie się okręgu o promieniu l i środku w punkcie jednej z krawędzi odcinka, a prostą prostopadła do środka odcinka
        bisection = self.getBisection()         
        possibleCircles = Circle(self.p1,l).getCommonPointsWithLine(bisection)

        #Wybrany zostaje odpowiedni punkt z dwóch możliwych
        #Jeżeli punkt p1 ma większą współrzędną Y wybrany jest punkt o współrzędnej X mniejszej.
        #Jeżeli punkt p1 ma mniejszą współrzędną Y wybrany jest punkt o współrzędnej X większej.
        if self.p1.Y > self.p2.Y:
            p3 = possibleCircles[0] if possibleCircles[0].X < possibleCircles[1].X else possibleCircles[1]
        elif self.p1.Y < self.p2.Y:
            p3 = possibleCircles[0] if possibleCircles[0].X > possibleCircles[1].X else possibleCircles[1]
        else:
            #Jeżeli punkty p1 i p2 mają tą samą wartość Y wybierany jest punkt na podstawie współrzędnej X.
            #Jeżeli p1.X < p2.X wbrany jest punkt o wartości Y mniejszej
            #W przeciwnym wypadku na odwrót.
            if self.p1.X < self.p2.X:
                p3 = possibleCircles[0] if possibleCircles[0].Y < possibleCircles[1].Y else possibleCircles[1]
            else:
                p3 = possibleCircles[0] if possibleCircles[0].Y > possibleCircles[1].Y else possibleCircles[1]
        self.middleOfCurve = p3
        

        #Konieczne jest też znalezienie punktu środka okręgu który opisuje krzywą.

        #Punkt ten jest punktem przecięcia się symetralnych odcinków tworzonych przez 3 punkty. 
        # Dowód: Okrąg z definicji jest zbiorem punktów oddalonych w tej samej odległości od środka. Symetralna odcinka jest zbiorem wszystkich punktów oddalonych w tej samej odległości od 
        #punktów leżących na krawędzi symetrycznych do symetralnej. W związku z tym przecięcie 2 symetralnych określa punkt oddalony w tej samej odległości od trzech punktów.

        bisection2 = Segment(self.p1,p3).getBisection()

        circleMiddle = bisection.getCommonPointWithLine(bisection2)
        circleMiddle = Point(round(circleMiddle.X,15),round(circleMiddle.Y,15))
        radius = circleMiddle.calculateDistance(self.p1)
        
        circle = AgrosCircle(len(circles)+1,circleMiddle,radius)

        # Sprawdzane jest czy okrąg znajduje się już w słowniku. Jeżeli nie jest do niego dodawany.
        self.circle = None
        for e in circles.values():
            if e == circle:
                self.circle = e
                
        if self.circle == None:
            self.circle = circle
            circles[self.circle.id] = self.circle
        


class Vector:
    """Klasa opisująca Vector2D

    Parameters:
        X (float): Opisuje wartość X wektora.
        Y (float): Opisuje wartość Y wektora.
    """
    def __init__(self,p1=None,p2=None,X=None,Y=None):
        """Konstruktor obiektu klasy Vector.
        Obiekt można stworzyć na 2 sposoby.
        Albo jawnie zdefiniować wartości X i Y wektora, albo podać punkty odcinka na podstwie którego wyliczane są wartości X i Y wektora.
        Jeżeli podane są obie wartości pierwszeństwo biorą argumenty X i Y.
        Args:
            p1 (Point, optional): Punkt odcinka. Defaults to None.
            p2 (Point, optional): Punkt odcinka. Defaults to None.
            X (float, optional): Wartość X wektora. Defaults to None.
            Y (float, optional): Wartość Y wektora. Defaults to None.
        """
        if X != None and Y != None:
            self.X = X
            self.Y = Y
        else:
            self.X = p2.X-p1.X
            self.Y = p2.Y-p1.Y

    def length(self):
        """Zwraca długość wektora

        Returns:
            float: Długość wektora.
        """
        return math.sqrt(self.X**2+self.Y**2)
    
    def dotProduct(self,other):
        """Zwraca Iloczyn skalarny dwóch wektorów.

        Args:
            other (Vector): Wektor przez który jest mnożony obiekt.

        Returns:
            float: Wartość iloczynu skalarnego Wektora
        """
        return self.X*other.X + self.Y*other.Y

    def det(self,other):
        """Zwraca wyznacznik dwóch wektorów

        Args:
            other (Vector): Wektor przez który jest obliczany wyznacznik.

        Returns:
            float: Wyznacznik wektorów.
        """
        return self.X*other.Y - self.Y*other.X

    def angleClockwise(self, other):
        """Zwraca kąt zgodnie z ruchem wskazówem zegara między tym wektorem a drugim wektorem.

        Args:
            other (Vector): Vector do którego wyliczany jest kąt.

        Returns:
            float: Wartość kąta liczona zgodnie z ruchem wskazóweg zegara w stopniach między wektorem z którego wywoływana jest funkcja a wektorem other.
        """
        cosx= self.dotProduct(other)/(self.length()*other.length())

        angle= math.degrees(math.acos(cosx))
        if self.det(other)<0: # Dzięki właściwości wyznacznika można określić czy kąt z iloczynu skalarnego jest zgodnie z ruchem wskazówek zwgara czy nie.
            return angle        #Jeżeli wyznacznik jest mniejszy od zera kąt już jest zgodny z ruchem wskazówek zegara.
        else:                   # W przeciwnym wypadku nie jest i należy od 360 odjąć wynik iloczynu skalarnego.
            return 360-angle

    def angleCounterClockwise(self, other):
        """Zwraca kąt przeciwnie do ruchu wskazówek zegara.

        Args:
            other (Vector): Vector do którego wyliczany jest kąt.

        Returns:
            float: Wartość kąta liczona przeciwnie z ruchem wskazóweg zegara w stopniach między wektorem z którego wywoływana jest funkcja a wektorem other.
        """
        return 360 - self.angleClockwise(other)  