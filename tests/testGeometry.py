import unittest

import sys
sys.path.append('..')


import geometry


class TestSegment(unittest.TestCase):
    def test_segment_correctly_says_if_point_is_in_segment(self):
        p1 = geometry.Point(0,0)
        p2 = geometry.Point(10,10)
        segment = geometry.Segment( p1,p2 )

        self.assertTrue(segment.isPointInSegment(geometry.Point(5,5) ) )
        self.assertTrue(segment.isPointInSegment(geometry.Point(0,0) ) )
        self.assertTrue(segment.isPointInSegment(geometry.Point(6,6) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(5,6) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(11,11) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(-2,-2) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(-5,6) ) )

        p1 = geometry.Point(10,10)
        p2 = geometry.Point(0,0)
        segment = geometry.Segment( p1,p2 )

        self.assertTrue(segment.isPointInSegment(geometry.Point(1,1) ) )
        self.assertTrue(segment.isPointInSegment(geometry.Point(5,5) ) )
        self.assertTrue(segment.isPointInSegment(geometry.Point(6,6) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(5,6) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(11,11) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(-2,-2) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(-5,6) ) )

        p1 = geometry.Point(10,10)
        p2 = geometry.Point(20,20)
        segment = geometry.Segment( p1,p2 )

        
        self.assertTrue(segment.isPointInSegment(geometry.Point(15,15) ) )
        self.assertTrue(segment.isPointInSegment(geometry.Point(11,11) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(5,5) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(6,6) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(5,6) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(-2,-2) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(-5,6) ) )

        p1 = geometry.Point(0,0)
        p2 = geometry.Point(0,10)
        segment = geometry.Segment( p1,p2 )

        self.assertTrue(segment.isPointInSegment(geometry.Point(0,5) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(0,15) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(1,5) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(0,-5) ) )

        p1 = geometry.Point(0,10)
        p2 = geometry.Point(0,0)
        segment = geometry.Segment( p1,p2 )

        self.assertTrue(segment.isPointInSegment(geometry.Point(0,5) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(0,15) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(1,5) ) )
        self.assertFalse(segment.isPointInSegment(geometry.Point(0,-5) ) )

unittest.main()