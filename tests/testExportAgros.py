import unittest

import sys
sys.path.append('..')


import exportAgros


class TestAgros2DProblem(unittest.TestCase):
    def test_Agros2DProblem_correctly_creates_fat_program(self):
        with open("a2d\\test.txt") as file:
            self.assertEqual(str(exportAgros.Agros2DProblem("a2d\\test.a2d")),file.read())

unittest.main()