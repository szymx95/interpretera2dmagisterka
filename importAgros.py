import xml.etree.ElementTree as ET
import re
import geometry
import sys, getopt
field_types = {"electrostatic":"esxy","magnetic":"msxy"}
material_types = {"electrostatic":"electrostatic_permittivity","magnetic":"magnetic_permeability"}
material_types_fields = {"electrostatic":"electrostatic_charge_density","magnetic":"magnetic_current_density_external_real"}
boundaryTypes = {'electrostatic':'electrostatic_potential','magnetic':'magnetic_potential_real'}

HELP_STRING = 'importAgros.py -i <sciezka pliku wejsciowego> \n Wartosci opcjonalne:\n\t-o <sciezka pliku wyjsciowego> Jeżeli wartość -o nie zostanie podana wygenerowany tekst zostanie wypisany na standardowe wyjscie\n\t--raster=<raster>\n\t--waga=<waga>'

class MacroElement:
    """Obiekt opisujący makro element w programie FAT.

    Parametry:
        id (int): id makro elementu.
        material (dict): Słownik opisujący materiał makro elementu.
        edges (list[geometry.AgrosSegment]): lista krawędzi makro elementu.
    """
    def __init__(self,label,edges):
        """Konstruktor obiektu MacroElement.
        Konstruktor przypisuje krawędzie z listy argumentu edges które opisują figurę wewnątrz której znajduje się label. Wpółrzędne argumentu label muszą znajdować się wewnątrz figury.
        Krawędzie nie mogą się przecinać w przeciwnym wypadku nie jest to poprawny program Agros2D. Jeżeli nie uda się znaleźć wszystkich odcinków figury parametr edges ustawiany jest jako pusta lista.


        Sposób znajdywania krawędzi:
            1. Algorytm znajduja krawędź początkowa [pk] - jest to najbliższe krawędź którą przecina półprosta z punktu label w kierunku Y+.
            2. Wybranie początkowego punktu (prawego) [pp] - jest to punkt o większej wartości X z punktów krawędzi pk, jeżeli X są równe, wybierany jest punkt o większym Y.
            3. Znalezienie wszystkich krawędzi które posiadają punkt pp, oprócz krawędzi pp
            4. Wybranie krawędzi która tworzy największy kąt (zgodnie z ruchem wskazówek zegara) z pk. (W pierwszym ruchu  przykładu k3)
            5. Ustawienie punktu pp jako drugiego punktu znalezionej krawędzi i dodanie krawędzi do parametru obiektu edges
            6. Powtarzaj punkty 3-5 aż punkt pp będzie punktem końcowym.
            7. Sprawdź czy punkt label znajduje się wewnątrz figury. Jeżeli nie usuń krawędzie z listy edges i zacznij od początku. Jeżeli tak zakończ.

                                                    początkowy punkt [pp]
                                       punkt końcowy        |   /
                                            |               |  / <- krawędź 1 [k1]
                                            V               V /
                początkowa krawędź [pk]->   ____|____________/
                                            |   |           |\\
                                            |   |           | \\  <- krawędź 2 [k2]
                                            |   ^           |  \\
                                            |   |           |   \\
                                            |   |           |
                                            |   |           | <- krawędź 3 [k3]
                                            |   .<-label    |
                                            |_______________|
        Args:
            label (geometry.AgrosLabel): Etykieta makro elementu.
            edges (list[geometry.AgrosSegment]): Lista wszystkich odcinków z programu Agros.
        """

        def CzyLiniaPoziomaPrzecinaKrawedz(p,p0,p1):
            if p0.Y > p1.Y:
                p0, p1 = p1, p0
            if p1.Y==p0.Y:
                return 0
            if p0.Y < p.Y and p1.Y <= p.Y:
                return 0
            if p0.Y > p.Y and p1.Y > p.Y:
                return 0
            if p0.X<p.X and p1.X < p.X:
                return 0
            if p0.X >= p.X and p1.X >= p.X:
                return 1
            t  = (p.Y-p0.Y)/(p1.Y-p0.Y)
            xp = p0.X + t*(p1.X-p0.X)
            if xp >= p.X:
                return 1
            else:
                return 0

        
        self.id = label.id
        self.material = label.material

        validPoint = False
        while validPoint == False:
            validPoint = False
            startingSegment = label.getClosestNorthSegmentFromList(edges)
            if startingSegment == None:
                self.edges = []
                return None

            self.edges = [startingSegment]

            nextPoints = [startingSegment.getRightPoint(),startingSegment]
            finishPoint = startingSegment.getOtherPoint(nextPoints[0])

            while nextPoints[0] != finishPoint:            
                nextPoints = nextPoints[1].getNextRightSegment(nextPoints[0],edges)
                if nextPoints == None:
                    self.edges = []
                    return None
                self.edges.append(nextPoints[1])

            suma = 0
            for edge in self.edges:
                suma += CzyLiniaPoziomaPrzecinaKrawedz(label, edge.p1, edge.getMiddle())
                suma += CzyLiniaPoziomaPrzecinaKrawedz(label, edge.getMiddle(), edge.p2)
            if suma % 2 == 1:
                validPoint = True
            else:
                edges = [e for e in edges if e not in self.edges]

class Agros2DProblem:
    """Klasa odpowiedzialna za wczytanie modelu Agros2D do zmiennych pythona.

    funkcja str() tej klasy zwraca program w FAT.
    
    Parameters:
        raster (float): Wartość raster ustalana przy wywołaniu programu z linii poleceń.
        waga (float): Wartość waga ustalana przy wywołaniu programu z linii poleceń.
        problemName (string): Nazwa problemy dla języka FAT.
        edges (list(geometry.AgrosSegment)): Lista odcinków zaimportowana z pliku a2d.
        nodes (list(geometry.AgrosPoint)): Lista punktów zaimportowana z pliku a2d.
        labels (list(geometry.AgrosLabel)): Lista etykiet zaimportowana z pliku a2d.
        boundaries (dict): Słownik zaimportowany z pliku XML a2d który zawiera informację o warunkach brzegowych.
        fields (dict): Słownik zaimportowany z pliku XML a2d który zawiera informację o polach symuylacji modelu.
        materials (dict): Słownik zaimportowany z pliku XML a2d który zawiera informację o materiałach.
        circles (list(geometry.AgrosCircle)): Lista okręgów zaimportowana z pliku a2d.
        macroElements (list(MacroElement)): Lista makro elementów.
    """
    def __init__(self,filePath,raster = 0.025, waga = 0):
        """Konstruktor obiektów klasy Agros2DProblem.
        Konstruktor przyjmuje ścieżkę do modelu (plików *.a2d) Agros2d.
        Konstruktor od razu analizuje plik, nie jest konieczne wywoływanie innych funkcji. 

        Args:
            filePath (string): Ścieżka do pliku modelu Agros2D (*.a2d)
            raster (float, optional): Raster makro elementów dla programu FAT. Defaults to 0.025.
            waga (float, optional): Waga makro elementów dla programu FAT. Defaults to 0.
        """
        self.raster = raster
        self.waga = waga
        xmlFile = open(filePath,'r').read()

        xmlFile = re.sub('id="(\d+?)"',lambda match: 'id="%d"' % (int(match.group(1))+1),xmlFile)
        xmlFile = re.sub('start="(\d+?)"',lambda match: 'start="%d"' % (int(match.group(1))+1),xmlFile)
        xmlFile = re.sub('end="(\d+?)"',lambda match: 'end="%d"' % (int(match.group(1))+1),xmlFile)

        root = ET.fromstring(xmlFile)

        xmlGeometry ={}

        xmlGeometry['nodes'] =  {}
        xmlGeometry['edges'] =  {}
        xmlGeometry['labels'] = {} 

        self.problemName = "agros"

        self.edges = {}
        self.nodes = {}
        self.labels = {}

        self.boundaries = {}
        self.fields ={}

        self.materials = {}




        attribIsInt = ['id','end','is_curvilinear','segments','start']

        for child in root:
            if child.tag == "geometry":
                for geometryChild in child:
                    for element in geometryChild:
                        id_xml = int(element.attrib['id'])
                        xmlGeometry[geometryChild.tag][id_xml] = {}
                        for attrib in element.attrib:
                            if attrib in attribIsInt:
                                xmlGeometry[geometryChild.tag][id_xml][attrib] = int(element.attrib[attrib])
                            else:
                                xmlGeometry[geometryChild.tag][id_xml][attrib] = float(element.attrib[attrib])
            if child.tag == 'problem':
                for problemChild in child:
                    if problemChild.tag == "fields":
                        for field in problemChild:
                            self.fields[field.attrib['field_id']] = field.attrib
                            for fieldChild in field:
                                if fieldChild.tag == 'boundaries':
                                    for boundary in fieldChild:
                                        id_xml = int(boundary.attrib['id'])
                                        self.boundaries[id_xml] = boundary.attrib
                                        self.boundaries[id_xml]['id'] = int(self.boundaries[id_xml]['id'])
                                        self.boundaries[id_xml]['boundaryEdges'] = []
                                        self.boundaries[id_xml]['boundaryTypes'] = {}
                                        for boundaryChild in boundary:
                                            if boundaryChild.tag == 'boundary_edges':
                                                for boundaryEdge in boundaryChild:
                                                    id_edge = int(boundaryEdge.attrib['id'])
                                                    self.boundaries[id_xml]['boundaryEdges'].append(id_edge)
                                                    xmlGeometry['edges'][id_edge]['boundary'] = id_xml
                                            if boundaryChild.tag == 'boundary_types':
                                                for boundaryType in boundaryChild:
                                                    self.boundaries[id_xml]['boundaryTypes'][boundaryType.attrib['key']] = float(boundaryType.attrib['value'])
                                if fieldChild.tag == 'materials':  
                                    for material in fieldChild:
                                        id_xml = int(material.attrib['id'])
                                        self.materials[id_xml] = material.attrib
                                        self.materials[id_xml]['id'] = int(self.materials[id_xml]['id'])
                                        self.materials[id_xml]['materialLabels'] = []
                                        self.materials[id_xml]['materialTypes'] = {}
                                        for materialChild in material:
                                            if materialChild.tag == 'material_labels':
                                                for materialLabel in materialChild:
                                                    id_label = int(materialLabel.attrib['id'])
                                                    self.materials[id_xml]['materialLabels'].append(id_label) 
                                                    xmlGeometry['labels'][id_label]['material'] = id_xml
                                            if materialChild.tag == 'material_types':
                                                for materialType in materialChild:
                                                    self.materials[id_xml]['materialTypes'][materialType.attrib['key']] = float(materialType.attrib['value'])

        self.circles = {}

        for e,node in zip(xmlGeometry['nodes'], xmlGeometry['nodes'].values()):
            self.nodes[e] = geometry.AgrosPoint(node['id'],node['x'],node['y'])

        for e,edge in zip(xmlGeometry['edges'], xmlGeometry['edges'].values()):
            if 'boundary' in edge:
                self.edges[e] = geometry.AgrosSegment(edge['id'], self.nodes[edge['start']],self.nodes[edge['end']], edge['angle'], edge['boundary'] )
            else:
                self.edges[e] = geometry.AgrosSegment(edge['id'], self.nodes[edge['start']],self.nodes[edge['end']], edge['angle'])
            if self.edges[e].angle != 0:
                self.edges[e].calculateCircle(self.circles)

        for e,label in zip(xmlGeometry['labels'],xmlGeometry['labels'].values()):
            if 'material' in label:
                self.labels[e] = geometry.AgrosLabel(label['id'], label['x'],label['y'],self.materials[label['material']])
            else:
                self.labels[e] = geometry.AgrosLabel(label['id'], label['x'],label['y'])

        self.macroElements = {}

        for label in self.labels.values():
            makro = MacroElement(label,self.edges.values())
            if makro.edges != []:
                self.macroElements[label.id] = makro
            
    def __str__(self):
        """Funkcja zwracająca tekst programu FAT.
        Każda krawędź posiada osobny segment i osobne przypisanie warunku brzegowego.

        Returns:
            string: Tekst programu FAT wygenerowany z modelu Agros2D.
        """
        fieldType = list(self.fields.values())[0]['field_id']

        returnString = "problem '%s'\n\n" % self.problemName
        
        returnString += "eqtn %s\n\n" % field_types[fieldType]
        
        for material in self.materials.values():
            if fieldType == 'electrostatic':
                returnString += "+mt%d [%s*eps0]\n" %(material['id'], material['materialTypes'][material_types[fieldType]] )
            if fieldType == 'magnetic':
                returnString += "+mt%d [%s/mu_0]\n" %(material['id'], material['materialTypes'][material_types[fieldType]] )            

        returnString += "\n\n"

        for node in self.nodes.values():
            returnString += "+bp%d %f %f\n" % (node.id,node.X,node.Y)

        returnString += "\n\n"

        for circle in self.circles.values():
            returnString += "+cr%d %f %f %f\n" % (circle.id,circle.X,circle.Y,circle.radius)
        
        returnString += "\n\n"
        
        for edge in self.edges.values():
            if edge.angle != 0:
                returnString += "\n+sg%d bp%d bp%d -%d\n+chkr%d sg%d\n\n" % (edge.id,edge.p2.id,edge.p1.id,edge.circle.id,edge.id,edge.id)
            else:
                returnString += "+chkr%d bp%d bp%d\n" % (edge.id,edge.p1.id,edge.p2.id)

        returnString += "\n\n"

        for macro in self.macroElements.values():
            if macro.material == None:
                returnString += "+meel%d( 0 0 %f %f ) %s\n" % ( macro.id,self.raster,self.waga, ' '.join([ 'kr%d'%(e.id) for e in macro.edges]) ) 
            else:                   
                returnString += "+meel%d( mt%d %s %f %f ) %s\n" % ( macro.id,macro.material['id'], macro.material['materialTypes'][material_types_fields[fieldType]], self.raster,self.waga, ' '.join([ 'kr%d'%(e.id) for e in macro.edges]) )
        
        returnString += "\n\n"

        for edge in self.edges.values():
            if edge.boundary != None:                
                returnString += "bnd kr%d diri %f\n" % (edge.id, self.boundaries[edge.boundary]['boundaryTypes'][boundaryTypes[fieldType]] )
        return returnString

    def saveToFile(self,filePath):
        with open(filePath,'w') as saveFile:
            saveFile.write(str(self))

def main(arguments):
    """Główna funkcja wywoływana przy uruchomieniu skryptu z linii poleceń

    importAgros.py -i <sciezka pliku wejsciowego> 
     Wartosci opcjonalne:
            -o <sciezka pliku wyjsciowego> Jeżeli wartość -o nie zostanie podana wygenerowany tekst zostanie wypisany na standardowe wyjscie
            --raster=<raster>
            --waga=<waga>'

    Args:
        arguments (list[string]): Lista argumentu programu.
    """
    inputfile = ''
    outputfile = ''
    raster = 0.025
    waga = 0
    try:
        opts, args = getopt.getopt(arguments,"hi:o:",["raster=","waga="])
        if opts == []:
            print(HELP_STRING)
            sys.exit(2)       
    except:
        print(HELP_STRING)
        sys.exit(2)        
    for opt, arg in opts:
        if opt == '-h':
            print(HELP_STRING)
            sys.exit(2)
        elif opt == "-i":
            inputfile = arg.replace("'",'').replace('"','')
        elif opt == "-o":
            outputfile = arg.replace("'",'').replace('"','')
        elif opt == '--raster':
            raster = float(arg)
        elif opt == '--waga':
            waga = float(arg)
            
    pr = Agros2DProblem(inputfile,raster,waga)
    if outputfile != '':
        pr.saveToFile(outputfile)
    else:
        print(pr)

    
if __name__ == '__main__':
    main(sys.argv[1:])