Autor: Szymon Szlachtowicz

Program na magisterkę 

Interpreter języka Agros2D do FAT

## Struktura Folderów w repozytorium

│   .gitignore                  - Lista plików ignorowanych przez GIT  
│   geometry.py                 - Plik odpowiedzialny za przeliczanie geometri z a2d do FAT  
│   importAgros.py              - Główny moduł odpowiedzialny za tworzenie plików .FAT  
│   README.md                   - Plik opisujący użycie interpretera  
│   __init__.py                 - Plik który określa folder jako moduł pythona  
│  
├───docs                        - Folder zawierający dokumentację programu  
├───examples                    - Folder zawierający przykładowe programy FAT  
└───tests                       - Folder zawierający testy jednostkowe  

## Jak używać

Aby uruchomić program należy skorzystać z linii poleceń (windows) lub terminalu.
Konieczne jest posiadanie zainstalowanego programu Python.

```
py importAgros.py -i <sciezka pliku wejsciowego> 
Wartosci opcjonalne:
        -o <sciezka pliku wyjsciowego> Jeżeli wartość -o nie zostanie podana wygenerowany tekst zostanie wypisany na standardowe wyjscie
        --raster=<raster>
        --waga=<waga>
```

## Dokumentacja

Dokumentacja znajduje się w folderze docs
Opisane są tam klasy w modułach geometry, importAgros.
plik index.html służy za pomoc w poruszaniu się między dokumentacjami.
Dokumentacja wygenerowana została z Docstrings każdej klasy i funkcji.